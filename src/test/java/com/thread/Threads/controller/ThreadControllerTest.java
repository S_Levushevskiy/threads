package com.thread.Threads.controller;

import com.thread.Threads.constant.Constant;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class ThreadControllerTest {

    @Test
    public void startThread() throws IOException, InterruptedException {
        ThreadController threadController = new ThreadController();
        for (int i = 1; i <= 8; i++) {
            threadController.startThread(i);
            File file = new File(Constant.URL_WRITE);
            file.delete();
        }
    }

}