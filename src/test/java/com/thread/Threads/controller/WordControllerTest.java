package com.thread.Threads.controller;

import com.thread.Threads.constant.Constant;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class WordControllerTest {

    @Test
    public void countWord() throws IOException, InterruptedException {
        WordController wordController = new WordController();
        for (int i = 1; i <= 8; i++) {
            wordController.countWord(i);
            File file = new File(Constant.URL_WRITE_WORD);
            file.delete();
        }
    }
}