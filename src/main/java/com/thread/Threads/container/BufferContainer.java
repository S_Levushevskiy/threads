package com.thread.Threads.container;

public abstract class BufferContainer<T> {

    public abstract void processData(T doFunction);

    public abstract void reset();

    public abstract String getData();
}
