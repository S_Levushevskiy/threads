package com.thread.Threads.container;

import com.thread.Threads.entity.sql.Discipline;
import com.thread.Threads.entity.sql.StudentStatistic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


public class DisciplineStatisticBuffer extends BufferContainer<Discipline> {

    public double fiveMarkStudentPercent;
    public double fiveOrFourMarkStudentPercent;
    public Map<Integer, Integer> markStatistic = new HashMap<>();
    public static Map<Long, Integer> studentMark = new HashMap<>();
    public Map<Long, StudentStatistic> studentStatisticMap = new HashMap<>();

    public DisciplineStatisticBuffer() {
        markStatistic.put(2, 0);
        markStatistic.put(3, 0);
        markStatistic.put(4, 0);
        markStatistic.put(5, 0);
    }


    @Override
    public void processData(Discipline discipline) {
        try {
            int mark = getMarkNum(discipline.getMark());
            addStudentMark(discipline);
            markStatistic.replace(mark, markStatistic.get(mark) + 1);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void reset() {

    }

    @Override
    public String getData() {
        return null;
    }

    private int getMarkNum(String mark) {
        mark = mark.replace("\"", "");
        return Integer.parseInt(mark);
    }


    private synchronized void addStudentMark(Discipline discipline) {
        int mark = getMarkNum(discipline.getMark());

        if (studentMark.containsKey(discipline.getStudentId())) {
            if (studentMark.get(discipline.getStudentId()) > getMarkNum(discipline.getMark())
                    && studentMark.get(discipline.getStudentId()) >= 4) {
                updateStudentMark(studentMark.get(discipline.getStudentId()), mark);
                studentMark.replace(discipline.getStudentId(), mark);
            }
        } else {
            studentMark.put(discipline.getStudentId(), mark);
            setStudentMark(mark);
            studentStatisticMap.put(discipline.getStudentId(), new StudentStatistic());
        }

        updateStudentStatistic(discipline.getStudentId(), mark);
    }

    private synchronized void updateStudentStatistic(Long id, int mark) {
        switch (mark) {
            case 2:
                studentStatisticMap.get(id).setTwoPercent(studentStatisticMap.get(id).getTwoPercent() + 1);
                break;
            case 3:
                studentStatisticMap.get(id).setThreePercent(studentStatisticMap.get(id).getThreePercent() + 1);
                break;
            case 4:
                studentStatisticMap.get(id).setFourPercent(studentStatisticMap.get(id).getFourPercent() + 1);
                break;
            case 5:
                studentStatisticMap.get(id).setFivePercent(studentStatisticMap.get(id).getFivePercent() + 1);
                break;
        }
        studentStatisticMap.get(id).setCountDiscipline(studentStatisticMap.get(id).getCountDiscipline() + 1);
    }

    private void setStudentMark(Integer mark) {
        switch (mark) {
            case 2:
            case 3:
                break;
            case 4:
                fiveOrFourMarkStudentPercent++;
                break;
            case 5: {
                fiveOrFourMarkStudentPercent++;
                fiveMarkStudentPercent++;
            }
            break;
        }
    }

    private void updateStudentMark(Integer oldMark, Integer mark) {
        switch (mark) {
            case 2:
            case 3: {
                fiveOrFourMarkStudentPercent--;
                if (oldMark == 5) fiveMarkStudentPercent--;
            }
            break;
            case 4:
                fiveMarkStudentPercent--;
                break;
            case 5:
                break;
        }
    }

}
