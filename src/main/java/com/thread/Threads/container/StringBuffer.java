package com.thread.Threads.container;

public class StringBuffer extends BufferContainer<Double> {

    String data;

    @Override
    public void processData(Double doFunction) {
        data += doFunction + "\n";
    }

    @Override
    public void reset() {
        data = "";
    }

    @Override
    public String getData() {
        return data;
    }


}
