package com.thread.Threads.container;

import java.util.HashMap;
import java.util.Map;

public class MapBuffer extends BufferContainer<String> {

    Map<String, Long> hashMap = new HashMap();

    @Override
    public void processData(String string) {
        if (hashMap.containsKey(string)) {
            hashMap.replace(string, hashMap.get(string) + 1);
        } else {
            hashMap.put(string, 1L);
        }
    }

    @Override
    public void reset() {

    }

    @Override
    public String getData() {
        return hashMap.toString();
    }
}
