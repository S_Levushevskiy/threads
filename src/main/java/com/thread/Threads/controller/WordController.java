package com.thread.Threads.controller;

import com.thread.Threads.constant.Constant;
import com.thread.Threads.container.BufferContainer;
import com.thread.Threads.container.MapBuffer;
import com.thread.Threads.function.StopWatch;
import com.thread.Threads.function.impl.WordFunction;
import com.thread.Threads.service.ThreadForWord;
import com.thread.Threads.service.WriteResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping("/api")
public class WordController {

    @GetMapping("/word/{threadNum}")
    public ResponseEntity countWord(@PathVariable int threadNum) throws IOException {
        StopWatch.setStartTime();
        BufferContainer bufferContainer = new MapBuffer();
        WriteResult writeResult = new WriteResult(new BufferedWriter(new FileWriter(Constant.URL_WRITE_WORD)));
        ExecutorService executorService = Executors.newFixedThreadPool(threadNum);
        File file = new File(Constant.URL_READ_WORD);
        for (File listFile : file.listFiles()) {
            executorService.submit(new ThreadForWord(listFile, bufferContainer, new WordFunction()));
        }

        executorService.shutdown();
        while (!executorService.isTerminated()) {
        }
        writeResult.write(bufferContainer.getData());
        writeResult.close();

        return ResponseEntity.ok(StopWatch.getResult());

    }
}
