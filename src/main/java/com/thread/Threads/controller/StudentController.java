package com.thread.Threads.controller;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.util.JSON;
import com.thread.Threads.constant.Constant;
import com.thread.Threads.container.DisciplineStatisticBuffer;
import com.thread.Threads.db.BasicConnectionPool;
import com.thread.Threads.db.StudentManagerNoSQL;
import com.thread.Threads.db.StudentManagerSQL;
import com.thread.Threads.function.StopWatch;
import com.thread.Threads.function.impl.StudentFunction;
import com.thread.Threads.service.ReadStudent;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api")
public class StudentController {

    @GetMapping("/student/{threadNum}")
    public ResponseEntity parseStudents(@PathVariable int threadNum) throws FileNotFoundException, SQLException {
        StopWatch.setStartTime();
        BasicConnectionPool basicConnectionPool = BasicConnectionPool.create(
                "jdbc:mysql://localhost:3306/students?verifyServerCertificate=false&useSSL=false&requireSSL=false&characterEncoding=UTF-8",
                "root", "root");
        DisciplineStatisticBuffer disciplineStatisticBuffer = new DisciplineStatisticBuffer();
        ExecutorService executorService = Executors.newFixedThreadPool(threadNum);
        File file = new File(Constant.URL_STUDENT);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        Stream<String> stringStream = bufferedReader.lines();
        List<String> stringList = stringStream.collect(Collectors.toList());
        stringList.remove(0);
        int offset = stringList.size() / threadNum;

        for (int i = 0; i < threadNum - 1; i++) {
            executorService.submit(new ReadStudent(stringList.subList(i * offset, i * offset + offset), new StudentFunction(new StudentManagerSQL(basicConnectionPool.getConnection())), disciplineStatisticBuffer));
        }
        executorService.submit(new ReadStudent(stringList.subList((threadNum - 1) * offset, stringList.size()), new StudentFunction(new StudentManagerSQL(basicConnectionPool.getConnection())), disciplineStatisticBuffer));

        executorService.shutdown();
        while (!executorService.isTerminated()) {
        }
        new StudentManagerSQL(basicConnectionPool.getConnection()).insertStatistic(disciplineStatisticBuffer);

        return ResponseEntity.ok(StopWatch.getResult());
    }

    @GetMapping("/mongodb/{threadNum}")
    public ResponseEntity insertMongo(@PathVariable int threadNum) throws UnknownHostException, FileNotFoundException {
        StopWatch.setStartTime();
        MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
        MongoClient mongoClient = new MongoClient(connectionString);


        DisciplineStatisticBuffer disciplineStatisticBuffer = new DisciplineStatisticBuffer();
        ExecutorService executorService = Executors.newFixedThreadPool(threadNum);
        File file = new File(Constant.URL_STUDENT);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        Stream<String> stringStream = bufferedReader.lines();
        List<String> stringList = stringStream.collect(Collectors.toList());
        stringList.remove(0);
        int offset = stringList.size() / threadNum;

        for (int i = 0; i < threadNum - 1; i++) {
            executorService.submit(new ReadStudent(stringList.subList(i * offset, i * offset + offset), new StudentFunction(new StudentManagerNoSQL(mongoClient)), disciplineStatisticBuffer));
        }
        executorService.submit(new ReadStudent(stringList.subList((threadNum - 1) * offset, stringList.size()), new StudentFunction(new StudentManagerNoSQL(mongoClient)), disciplineStatisticBuffer));

        executorService.shutdown();
        while (!executorService.isTerminated()) {
        }
        new StudentManagerNoSQL(mongoClient).insertStatistic(disciplineStatisticBuffer);


        return ResponseEntity.ok(StopWatch.getResult());
    }


}
