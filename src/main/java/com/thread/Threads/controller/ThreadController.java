package com.thread.Threads.controller;

import com.thread.Threads.constant.Constant;
import com.thread.Threads.container.BufferContainer;
import com.thread.Threads.function.impl.NumberFunction;
import com.thread.Threads.service.ThreadForNumber;
import com.thread.Threads.function.StopWatch;
import com.thread.Threads.container.StringBuffer;
import com.thread.Threads.service.WriteResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.thread.Threads.constant.Constant.URL_READ;

@RestController
@RequestMapping("/api")
public class ThreadController {

    @GetMapping("/thread/{threadNum}")
    public ResponseEntity startThread(@PathVariable int threadNum) throws InterruptedException, IOException {
        StopWatch.setStartTime();
        BufferContainer bufferContainer = new StringBuffer();
        WriteResult writeResult = new WriteResult(new BufferedWriter(new FileWriter(Constant.URL_WRITE)));
        ExecutorService executorService = Executors.newFixedThreadPool(threadNum);
        for (int i = 1; i <= 3; i++) {
            File file = new File(URL_READ + i);
            for (File listFile : file.listFiles()) {
                executorService.submit(new ThreadForNumber(listFile, writeResult, bufferContainer, new NumberFunction()));
            }
        }
        executorService.shutdown();
        while (!executorService.isTerminated()) {
        }
        writeResult.close();

        return ResponseEntity.ok(StopWatch.getResult());
    }

}
