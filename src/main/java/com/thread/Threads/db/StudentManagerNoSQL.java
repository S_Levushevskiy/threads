package com.thread.Threads.db;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;
import com.thread.Threads.container.DisciplineStatisticBuffer;
import com.thread.Threads.entity.sql.Discipline;
import com.thread.Threads.entity.sql.Student;
import com.thread.Threads.entity.sql.StudentStatistic;

import java.util.Map;

public class StudentManagerNoSQL implements DatabaseManager {

    private final DBCollection studentCollection;
    private final DBCollection disciplineCollection;
    private final MongoClient mongoClient;

    public StudentManagerNoSQL(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
        studentCollection = mongoClient.getDB("student").getCollection("student");
        disciplineCollection = mongoClient.getDB("discipline").getCollection("discipline");
    }

    @Override
    public void insertDiscipline(Discipline discipline) {
        DBObject dbObject = (DBObject) JSON.parse(discipline.toString());
        disciplineCollection.insert(dbObject);
    }

    @Override
    public void insertStudent(Student student) {
        if (!DisciplineStatisticBuffer.studentMark.containsKey(student.getId())) {
            try {
                DBObject dbObject = (DBObject) JSON.parse(student.toString());
                studentCollection.insert(dbObject);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void insertStatistic(DisciplineStatisticBuffer disciplineStatisticBuffer) {
        insertUniversityStatistic(disciplineStatisticBuffer.markStatistic,
                disciplineStatisticBuffer.fiveMarkStudentPercent,
                disciplineStatisticBuffer.fiveOrFourMarkStudentPercent,
                disciplineStatisticBuffer.studentStatisticMap.size());

        insertStudentStatistic(disciplineStatisticBuffer.studentStatisticMap);
    }

    private void insertUniversityStatistic(Map<Integer, Integer> markStatistic, double fiveMarkStudentPercent, double fiveOrFourMarkStudentPercent, int size) {
        int total = markStatistic.get(5) + markStatistic.get(4) + markStatistic.get(3) + markStatistic.get(2);
        String json = "{ " +
                "fiveMarkStudentPercent:" + fiveMarkStudentPercent / size +
                ", fiveOrFourMarkStudentPercent:" + fiveOrFourMarkStudentPercent / size +
                ", fivePercent: " + (double)markStatistic.get(5) / total +
                ", fourPercent: " + (double)markStatistic.get(4) / total +
                ", threePercent: " +(double) markStatistic.get(3) / total +
                ", twoPercent: " + (double)markStatistic.get(2) / total + " }";
        DBCollection universityStatistic = mongoClient.getDB("statistic").getCollection("university");
        DBObject dbObject = (DBObject) JSON.parse(json);
        universityStatistic.insert(dbObject);
    }

    public void insertStudentStatistic(Map<Long, StudentStatistic> studentStatisticMap) {
        DBCollection studentStatistic = mongoClient.getDB("statistic").getCollection("student");

        studentStatisticMap.entrySet().forEach(student -> {
            DBObject dbObject = (DBObject) JSON.parse("{ " +
                    "_id:" + student.getKey() +
                    ", fivePercent: " + student.getValue().getFivePercent() / student.getValue().getCountDiscipline() +
                    ", fourPercent: " + student.getValue().getFourPercent() / student.getValue().getCountDiscipline() +
                    ", threePercent: " + student.getValue().getThreePercent() / student.getValue().getCountDiscipline() +
                    ", twoPercent: " + student.getValue().getTwoPercent() / student.getValue().getCountDiscipline() + " }");
            studentStatistic.insert(dbObject);
        });
    }
}
