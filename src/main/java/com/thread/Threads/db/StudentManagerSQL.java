package com.thread.Threads.db;

import com.thread.Threads.container.DisciplineStatisticBuffer;
import com.thread.Threads.entity.sql.Discipline;
import com.thread.Threads.entity.sql.Student;
import com.thread.Threads.entity.sql.StudentStatistic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

public class StudentManagerSQL implements DatabaseManager {


    private final String sql_discipline = "INSERT INTO students.discipline (etsc, control_form, mark, name, points, student_id, term_id, year_number) " +
            "VALUES (?, ?, ?,?, ?, ?, ?, ?);";

    private final String sql_student = "INSERT INTO students.student (id, name)" +
            " VALUES (?, ?);";

    private final String sql_university_statistic =
            "INSERT INTO students.university_statistic (student_percent_of_five, student_percent_of_five_or_four, five_percent, four_percent, three_percent, two_percent) " +
                    "VALUES (?, ?, ?, ?, ?, ?);";
    private final String sql_student_statistic =
            "INSERT INTO students.student_statistic (id, five_percent, four_percent, three_percent, two_percent)" +
                    " VALUES (?, ?, ?, ?, ?);";

    private final Connection connection;

    public StudentManagerSQL(Connection connection) {
        this.connection = connection;
    }

    public void insertDiscipline(Discipline discipline) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql_discipline);
            preparedStatement.setString(1, discipline.getETSC());
            preparedStatement.setString(2, discipline.getControlForm());
            preparedStatement.setString(3, discipline.getMark());
            preparedStatement.setString(4, discipline.getName());
            preparedStatement.setString(5, discipline.getPoints());
            preparedStatement.setLong(6, discipline.getStudentId());
            preparedStatement.setString(7, discipline.getTermId());
            preparedStatement.setString(8, discipline.getYearNumber());
            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertStudent(Student student) {
        if (!DisciplineStatisticBuffer.studentMark.containsKey(student.getId())) {
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(sql_student);
                preparedStatement.setLong(1, student.getId());
                preparedStatement.setString(2, student.getName());
                preparedStatement.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void insertStatistic(DisciplineStatisticBuffer disciplineStatisticBuffer) {
        insertUniversityStatistic(disciplineStatisticBuffer.markStatistic,
                disciplineStatisticBuffer.fiveMarkStudentPercent,
                disciplineStatisticBuffer.fiveOrFourMarkStudentPercent,
                disciplineStatisticBuffer.studentStatisticMap.size());

        insertStudentStatistic(disciplineStatisticBuffer.studentStatisticMap);
    }

    private void insertUniversityStatistic(Map<Integer, Integer> markStatistic, double fiveMarkStudentPercent, double fiveOrFourMarkStudentPercent, int size) {
        int total = markStatistic.get(5) + markStatistic.get(4) + markStatistic.get(3) + markStatistic.get(2);
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql_university_statistic);
            preparedStatement.setDouble(1, fiveMarkStudentPercent / size);
            preparedStatement.setDouble(2, fiveOrFourMarkStudentPercent / size);
            preparedStatement.setDouble(3, (double) markStatistic.get(5) / total);
            preparedStatement.setDouble(4, (double) markStatistic.get(4) / total);
            preparedStatement.setDouble(5, (double) markStatistic.get(3) / total);
            preparedStatement.setDouble(6, (double) markStatistic.get(2) / total);
            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void insertStudentStatistic(Map<Long, StudentStatistic> studentStatisticMap) {
        studentStatisticMap.entrySet().forEach(student -> {
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(sql_student_statistic);
                preparedStatement.setLong(1, student.getKey());
                preparedStatement.setDouble(2, student.getValue().getFivePercent() / student.getValue().getCountDiscipline());
                preparedStatement.setDouble(3, student.getValue().getFourPercent() / student.getValue().getCountDiscipline());
                preparedStatement.setDouble(4, student.getValue().getThreePercent() / student.getValue().getCountDiscipline());
                preparedStatement.setDouble(5, student.getValue().getTwoPercent() / student.getValue().getCountDiscipline());
                preparedStatement.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        });
    }
}
