package com.thread.Threads.db;

import com.thread.Threads.container.DisciplineStatisticBuffer;
import com.thread.Threads.entity.sql.Discipline;
import com.thread.Threads.entity.sql.Student;

public interface DatabaseManager<E> {

    void insertDiscipline(Discipline discipline);

    void insertStudent(Student student);

    void insertStatistic(DisciplineStatisticBuffer disciplineStatisticBuffer);
}
