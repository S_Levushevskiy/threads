package com.thread.Threads.function;

import com.thread.Threads.container.BufferContainer;

public interface Function {

    public void doFunction(BufferContainer bufferContainer, String... strings);
}
