package com.thread.Threads.function;

import java.util.logging.Logger;

public class StopWatch {
    private static Logger logger = Logger.getLogger(StopWatch.class.getName());

    private static long startTime;
    private static long endTime;

    public static void setStartTime() {
        startTime = System.currentTimeMillis();
        logger.info("Start time is set: " + startTime);
    }

    public static void setEndTime() {
        endTime = System.currentTimeMillis();
        //logger.info("End time is set: " + endTime);
    }

    public static String getResult() {
        logger.info("Result time: " + (endTime - startTime));
        return "Result time: " + (endTime - startTime);
    }

}
