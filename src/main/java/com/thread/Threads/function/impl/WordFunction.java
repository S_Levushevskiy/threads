package com.thread.Threads.function.impl;

import com.thread.Threads.container.BufferContainer;
import com.thread.Threads.function.Function;

public class WordFunction implements Function {

    @Override
    public void doFunction(BufferContainer bufferContainer, String... strings) {
        for (String string : strings) {
            bufferContainer.processData(string);
        }
    }
}
