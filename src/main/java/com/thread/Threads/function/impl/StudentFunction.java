package com.thread.Threads.function.impl;

import com.thread.Threads.container.BufferContainer;
import com.thread.Threads.db.DatabaseManager;
import com.thread.Threads.entity.sql.Discipline;
import com.thread.Threads.entity.sql.Student;
import com.thread.Threads.function.Function;

public class StudentFunction implements Function {

    DatabaseManager databaseManager;


    public StudentFunction(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    @Override
    public void doFunction(BufferContainer bufferContainer, String... strings) {
        Discipline discipline = parse(strings);
        databaseManager.insertDiscipline(discipline);
        bufferContainer.processData(discipline);
    }

    public Discipline parse(String... strings) {
        Student student = new Student();
        student.setId(Long.parseLong(strings[1]));
        student.setName(strings[0]);

        Discipline disciplineName = new Discipline();
        disciplineName.setName(strings[2]);
        disciplineName.setControlForm(strings[3]);
        disciplineName.setYearNumber(strings[4]);
        disciplineName.setTermId(strings[5]);
        disciplineName.setMark(strings[6]);
        disciplineName.setPoints(strings[7]);
        disciplineName.setETSC(strings[8]);
        disciplineName.setStudentId(student.getId());
        databaseManager.insertStudent(student);
        student.setDisciplineNames(disciplineName);
        return disciplineName;
    }
}
