package com.thread.Threads.function.impl;

import com.thread.Threads.function.Function;
import com.thread.Threads.container.BufferContainer;

public class NumberFunction implements Function {

    int a, b, c, d;

    public NumberFunction() {
    }

    public NumberFunction(int a, int b, int c, int d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public NumberFunction(String... strings) {
        a = Integer.parseInt(strings[0]);
        b = Integer.parseInt(strings[1]);
        c = Integer.parseInt(strings[2]);
        d = Integer.parseInt(strings[3]);
    }

    public void parse(String... strings) {
        a = Integer.parseInt(strings[0]);
        b = Integer.parseInt(strings[1]);
        c = Integer.parseInt(strings[2]);
        d = Integer.parseInt(strings[3]);
    }

    /*
    (4*lg(c)-d/2+23)/a*a-b
     */
    public double doFunction() {
        return (4 * Math.log10(c) - d / 2 + 23) / (a * a - b);
    }

    @Override
    public void doFunction(BufferContainer bufferContainer, String... strings) {
        parse(strings);
        bufferContainer.processData(doFunction());
    }
}
