package com.thread.Threads.constant;

public class Constant {
    public static final String URL_READ = "/home/teck-stack/Documents/TestProject/Threads/src/main/resources/static/input_data";
    public static final String URL_WRITE = "/home/teck-stack/Documents/TestProject/Threads/src/main/resources/static/output.txt";
    public static final String URL_READ_WORD = "/home/teck-stack/Documents/TestProject/Threads/src/main/resources/static/words";
    public static final String URL_WRITE_WORD = "/home/teck-stack/Documents/TestProject/Threads/src/main/resources/static/result.txt";
    public static final String URL_STUDENT = "/home/teck-stack/Documents/TestProject/Threads/src/main/resources/static/export.txt";

}
