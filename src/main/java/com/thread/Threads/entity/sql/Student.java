package com.thread.Threads.entity.sql;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@EqualsAndHashCode
@Data
public class Student implements Serializable {

    @Id
    Long id;

    String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "studentId")
    List<Discipline> disciplineNames = new ArrayList<>();

    public void setDisciplineNames(Discipline disciplineName) {
        disciplineNames.add(disciplineName);
    }

    @Override
    public String toString() {
        return "{" +
                "_id:" + id +
                ", name:" + name +
                '}';
    }
}
