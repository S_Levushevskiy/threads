package com.thread.Threads.entity.sql;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@EqualsAndHashCode
@Data
public class StudentStatistic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    Double fivePercent = 0d;

    Double fourPercent = 0d;

    Double threePercent = 0d;

    Double twoPercent = 0d;

    Integer countDiscipline = 0;

}
