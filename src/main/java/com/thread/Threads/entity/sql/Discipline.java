package com.thread.Threads.entity.sql;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@EqualsAndHashCode
public class Discipline {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    String controlForm;

    String yearNumber;

    String termId;

    String mark;

    String points;

    String ETSC;

    Long studentId;

    public String getSql() {
        String sql = "INSERT INTO students.discipline (etsc, control_form, mark, name, points, student_id, term_id, year_number) " +
                "VALUES (\"" + ETSC + "', \"" + controlForm + "', \"" + mark + "', \"" + name + "', \"" + points + "', \"" + studentId + "', \"" + termId + "', \"" + yearNumber + "');";
        return sql;
    }

    @Override
    public String toString() {
        String tmp = name.replace("\"\"", "\'");
        return "{" +
                "name:" + tmp +
                ", controlForm:" + controlForm +
                ", yearNumber:" + yearNumber +
                ", termId:" + termId +
                ", mark:" + mark +
                ", points:" + points +
                ", ETSC:" + ETSC +
                ", studentId:" + studentId +
                '}';
    }

}
