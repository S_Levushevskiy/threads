package com.thread.Threads.entity.sql;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@EqualsAndHashCode
public class UniversityStatistic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    Double studentPercentOfFive;

    Double studentPercentOfFiveOrFour;

    Double fivePercent;

    Double fourPercent;

    Double threePercent;

    Double twoPercent;



}
