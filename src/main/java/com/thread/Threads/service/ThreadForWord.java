package com.thread.Threads.service;

import com.thread.Threads.container.BufferContainer;
import com.thread.Threads.function.Function;
import com.thread.Threads.function.StopWatch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ThreadForWord extends Thread {

    private final File file;
    private BufferContainer bufferContainer;
    private Function function;

    public ThreadForWord(File file, BufferContainer bufferContainer, Function function) {
        this.file = file;
        this.bufferContainer = bufferContainer;
        this.function = function;
    }

    public void run() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String read;

            while ((read = bufferedReader.readLine()) != null) {
                String[] strings = read.split(" ");
                function.doFunction(bufferContainer, strings);
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            StopWatch.setEndTime();
        }
    }

}
