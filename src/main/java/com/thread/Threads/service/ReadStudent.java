package com.thread.Threads.service;

import com.thread.Threads.container.BufferContainer;
import com.thread.Threads.entity.sql.Student;
import com.thread.Threads.function.Function;
import com.thread.Threads.function.StopWatch;

import java.util.ArrayList;
import java.util.List;

public class ReadStudent extends Thread {

    private final List<String> stringList;
    private final Function function;
    private final BufferContainer bufferContainer;
    public static List<Student> students = new ArrayList<>();

    public ReadStudent(List<String> stringList, Function function, BufferContainer bufferContainer) {
        this.stringList = stringList;
        this.function = function;
        this.bufferContainer = bufferContainer;
    }

    public void run() {
        try {
            stringList.forEach(string -> {
                String[] strings = string.split("\t");
                function.doFunction(bufferContainer, strings);
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            StopWatch.setEndTime();
        }
    }

}
