package com.thread.Threads.service;

import com.thread.Threads.container.BufferContainer;
import com.thread.Threads.function.Function;
import com.thread.Threads.function.StopWatch;

import java.io.*;
import java.util.logging.Logger;

public class ThreadForNumber extends Thread {

    private final File file;
    private WriteResult writeResult;
    private BufferContainer bufferContainer;
    private Function function;

    public ThreadForNumber(File file, WriteResult writeResult, BufferContainer bufferContainer, Function function) {
        this.writeResult = writeResult;
        this.file = file;
        this.bufferContainer = bufferContainer;
        this.function = function;
    }

    public void run() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String read;

            for (int counter = 1; (read = bufferedReader.readLine()) != null; counter++) {
                String[] strings = read.split(" ");
                function.doFunction(bufferContainer, strings);
                if (counter > 10){
                    writeResult.write(bufferContainer.getData());
                    bufferContainer.reset();
                    counter = 0;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writeResult.write(bufferContainer.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
            StopWatch.setEndTime();
        }
    }
}
