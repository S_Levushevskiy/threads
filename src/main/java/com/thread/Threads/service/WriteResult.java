package com.thread.Threads.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class WriteResult {
    private final BufferedWriter bufferedWriter;

    public WriteResult(BufferedWriter bufferedWriter) {
        this.bufferedWriter = bufferedWriter;
    }

    public synchronized void write(String data) throws IOException {
        bufferedWriter.write(data);
    }

    public void close() throws IOException {
        bufferedWriter.close();
    }
}